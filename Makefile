PHONY: all

build-retrieval: retrieval.Dockerfile
	docker build -f retrieval.Dockerfile -t registry.gitlab.com/smith-phep/test/dummy .

push-retrieval: build-retrieval
	docker push registry.gitlab.com/smith-phep/test/dummy

build-analysis: analysis.Dockerfile
	docker build -f analysis.Dockerfile -t registry.gitlab.com/smith-phep/test/dummy-analysis .

push-analysis: build-analysis
	docker push registry.gitlab.com/smith-phep/test/dummy-analysis

push-retrieval-tag: build-retrieval
	docker tag registry.gitlab.com/smith-phep/test/dummy:latest registry.gitlab.com/smith-phep/test/dummy:ver
	docker push registry.gitlab.com/smith-phep/test/dummy:ver

push-analysis-tag:
	docker tag registry.gitlab.com/smith-phep/test/dummy-analysis:latest registry.gitlab.com/smith-phep/test/dummy-analysis:ver
	docker push registry.gitlab.com/smith-phep/test/dummy-analysis:ver

all: push-retrieval push-analysis push-retrieval-tag push-analysis-tag
