FROM ubuntu:20.04

CMD ["/bin/bash", "-c", "printenv > /mnt/outputLocal/retrieval-done && printenv > /mnt/outputGlobal/retrieval-done && sleep ${SLEEP:-infinity}"]
