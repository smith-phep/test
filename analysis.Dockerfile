FROM ubuntu:20.04

CMD ["/bin/bash", "-c", "printenv > /mnt/outputLocal/analysis-done && printenv > /mnt/outputGlobal/analysis-done && sleep ${SLEEP:-infinity}"]
